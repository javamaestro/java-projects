package com.wsdl.server.endpoints;

import com.wsdl.server.model.Comment;
import com.wsdl.server.model.Feed;
import com.wsdl.server.model.User;
import com.wsdl.server.repos.CommentRepository;
import com.wsdl.server.repos.FeedRepository;
import com.wsdl.server.repos.UserRepository;
import com.wsdl.server.service.TypeConverter;
import io.spring.guides.gs_producing_web_service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.crypto.Data;
import javax.xml.datatype.DatatypeConfigurationException;
import java.lang.management.OperatingSystemMXBean;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Endpoint
public class CommentEndpoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";
    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final FeedRepository feedRepository;
    private final TypeConverter typeConverter;

    @Autowired
    public CommentEndpoint(CommentRepository commentRepository, UserRepository userRepository, FeedRepository feedRepository, TypeConverter typeConverter) {
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
        this.feedRepository = feedRepository;
        this.typeConverter = typeConverter;
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllCommentRequest")
    @ResponsePayload
    public GetAllCommentsResponse getAllCommentsResponse(@RequestPayload GetAllCommentRequest request) throws Exception {
        List<Comment> comments = commentRepository.findAllByFeedId(request.getFeedId());
        GetAllCommentsResponse response = new GetAllCommentsResponse();
        CommentList commentList = new CommentList();
        commentList.getComments().addAll(typeConverter.convertComments(comments));
        response.setCommentList(commentList);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "setCommentRequest")
    @ResponsePayload
    public void setCommentResponse(@RequestPayload SetCommentRequest request){
        SetCommentResponse response = new SetCommentResponse();
        if (request != null) {
            Comment comment = new Comment(request.getComment());
            commentRepository.save(comment);
        }
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "changeRatingRequest")
    @ResponsePayload
    public void changeRatingUpRequest(@RequestPayload ChangeRatingRequest request) {
        User user = null;
        Optional<User> currentUser = userRepository.findById(UUID.fromString(request.getUserID()));
        if(currentUser.isPresent()){
             user = currentUser.get();
        }else if(!currentUser.isPresent()) {
            user = new User(UUID.fromString(request.getUserID()));
        }
        userRepository.save(user);
        Comment comment = commentRepository.findById(request.getCommentId()).get();
        if (request.getFlag().equals("up")) {
            if (comment.getLikes().isEmpty() || !comment.getLikes().contains(currentUser)) {
                comment.getLikes().add(user);
                comment.setRating(comment.getRating() + 1);
                if (comment.getDislikes().contains(user)) {
                    comment.getDislikes().remove(user);
                    comment.setRating(comment.getRating() + 1);
                }
            }
        } else if (request.getFlag().equals("down")) {
            if (comment.getDislikes().isEmpty() || !comment.getDislikes().contains(currentUser)) {
                comment.getDislikes().add(user);
                comment.setRating(comment.getRating() - 1);
                if (comment.getLikes().contains(user)) {
                    comment.getLikes().remove(user);
                    comment.setRating(comment.getRating() - 1);
                }
            }
        }
        commentRepository.save(comment);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "returnRatingRequest")
    @ResponsePayload
    public void returnRating(@RequestPayload ReturnRatingRequest request) {
        User user = null;
        Optional<User> currentUser = userRepository.findById(UUID.fromString(request.getUserID()));
        if(currentUser.isPresent()){
            user = currentUser.get();
        }else if(!currentUser.isPresent()) {
            user = new User(UUID.fromString(request.getUserID()));
        }
        userRepository.save(user);
        Comment comment = commentRepository.findById(request.getCommentId()).get();
        if (request.getFlag().equals("like")) {
            comment.getLikes().remove(user);
            comment.setRating(comment.getRating() - 1);
        } else if (request.getFlag().equals("dislike")) {
            comment.getDislikes().remove(user);
            comment.setRating(comment.getRating() + 1);
        }
        commentRepository.save(comment);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteCommentRequest")
    @ResponsePayload
    public void deleteCommentRequest(@RequestPayload DeleteCommentRequest request) {
        commentRepository.deleteById(request.getCommentId());
    }
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "findFeedByDateRequest")
    @ResponsePayload
    public FindFeedByDateResponse findFeedByDateRequest(@RequestPayload FindFeedByDateRequest request) throws DatatypeConfigurationException {
        Date time = request.getDate().toGregorianCalendar().getTime();
        List<Feed> feedsByDate = feedRepository.findAllByCreationTime(time);
        FindFeedByDateResponse response = new FindFeedByDateResponse();
        FeedList feedList = new FeedList();
        feedList.getFeeds().addAll(typeConverter.convertToDetails(feedsByDate));
        response.setFeedList(feedList);
        return response;
    }
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "editCommentRequest")
    @ResponsePayload
    public void editComment(@RequestPayload EditCommentRequest request){
        Comment comment = commentRepository.findById(request.getCommentId()).get();
        comment.setText(request.getComment().getText());
        commentRepository.save(comment);
    }



}
