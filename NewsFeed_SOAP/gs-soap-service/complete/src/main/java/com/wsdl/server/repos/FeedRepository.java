package com.wsdl.server.repos;


import com.wsdl.server.model.Feed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface FeedRepository extends CrudRepository<Feed, Long> {
    Optional<Feed> findById(Long id);

    List<Feed> findAll();

    void deleteById(Long id);
    List<Feed> findAllByCreationTime(Date creationTime);
}
