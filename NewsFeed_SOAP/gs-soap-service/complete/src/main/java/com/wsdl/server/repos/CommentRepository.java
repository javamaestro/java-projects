package com.wsdl.server.repos;

import com.wsdl.server.model.Comment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends CrudRepository<Comment, Long> {
    List<Comment> findAllByFeedId(Long feedId);
    Optional<Comment> findById(Long commentId);
    List<Comment> findAll();
}
