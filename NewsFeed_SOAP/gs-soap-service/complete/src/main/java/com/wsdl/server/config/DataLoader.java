package com.wsdl.server.config;

import com.wsdl.server.model.Feed;
import com.wsdl.server.repos.FeedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.Random;

@Component
public class DataLoader {
    private final FeedRepository feedRepository;
    private String[] newsTitles = {"«Русские отступили, а мы проиграли»", "«Это не наркотик, но, боюсь, следствие закроет на это глаза»", "How Bitcoin Could Hit $50,000 in Just 9 Similar Rallies", "Chinese Bank Invests in Bitcoin Wallet After President Xi’s Remarks: Report", "Tron Price Up 25% as China Reportedly ‘Bans’ Anti-Blockchain Sentiment", "Liquid.com to Host Initial Exchange Offering for SUKU, which Empowers Conscious Consumers", "Tron (TRX) Surges On Massive Partnership Plug, China’s Latest Crypto Rankings"};
    private String[] newsContent = {"Bitcoin has had one of its best performing weekends of the year. The only other time it has moved as much in such a short period of time was in late June when BTC surged from $11k to its 2019 peak. It would not take many more movements of this magnitude to re… [+2981 chars]",  "On Friday, Chinas leader, President Xi Jinping, absolutely floored the Bitcoin community. Speaking at a meeting of the Political Bureau of the Chinese Communist Partys Central Committee, Xi called for the adoption of blockchain as an important breakthrough fo… [+3088 chars]", "China is rapidly adopting a pro-blockchain stance following news it would bring in a so-called “crypto law” in January next year. \r\nChina: Blockchain must be legitimate\r\nIn a sign of China’s abrupt official about-turn on blockchain, officials are deleting pos… [+1732 chars]", "THE SECURITIES BEING OFFERED HAVE NOT BEEN REGISTERED UNDER THE SECURITIES ACT OF 1933, AS AMENDED, AND MAY NOT BE OFFERED OR SOLD IN THE UNITED STATES OR TO U.S. PERSONS (AS SUCH TERM IS DEFINED IN RULE 902 AS PROMULGATED BY THE U.S. SECURITIES AND EXCHANGE … [+5957 chars]", "The news has all been China this weekend and its native crypto assets have been on fire. From bully to hero, Beijing has boosted crypto markets by 25% in just two days. Bitcoin led the rally but a slew of Chinese crypto coins are surging today, and Tron is on… [+3617 chars]", "Later this week the government will release the monthly jobs report for October at a time when a new survey says business hiring is slowing.\r\nHiring by U.S. companies has fallen to a seven-year low and fewer employers are raising pay, a business survey has fo… [+2787 chars]",    "General Motor's four-year deal will now be used as a template in bargaining with crosstown rival Ford Motor, the union's choice for the next round of bargaining, followed by Fiat Chrysler.\r\n“We can confirm the UAW today notified Ford it plans to negotiate wit… [+2605 chars]","October 28th, 2019 by Zachary Shahan \r\nThe most popular CleanTechnica stories of the past week were led by a (somewhat wild) claim of an aluminum-air battery breakthrough. Yup, really. After that, it was almost all Tesla Tesla Model 3 sales compared to other … [+2482 chars]"};
    private String[] newsAuthors = {"Emily Price", "John Week", "Arnold Schwarzenegger", "Maria Shriver","Marilyn Monroe", "Abraham Lincoln", "Nelson Mandela ","John F. Kennedy "};

    @Autowired
    public DataLoader(FeedRepository feedRepository) {
        this.feedRepository = feedRepository;
    }


    public void loadData() throws IOException {
        for (int i = 0; i < 20; i++) {
            Feed feed = new Feed();
            feed.setTitle(newsTitles[new Random().nextInt(newsTitles.length)]);
            feed.setContent(newsContent[new Random().nextInt(newsContent.length)]);
            feed.setAuthor(newsAuthors[new Random().nextInt(newsAuthors.length)]);
            feed.setnRating(0);
            feed.setCreationTime(new Date());
            feed.setImage(loadImage());
            feedRepository.save(feed);
        }
    }
    private byte[] loadImage() throws IOException {
        String dirName = "/home/kadymov/Pictures/" + (new Random().nextInt(9));
        File imagePath = new File(dirName);
        byte[] fileContent = Files.readAllBytes(imagePath.toPath());
        return fileContent;
    }

}
