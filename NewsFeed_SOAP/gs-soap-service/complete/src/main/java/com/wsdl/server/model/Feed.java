package com.wsdl.server.model;


import io.spring.guides.gs_producing_web_service.FeedDetails;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "feeds")
public class Feed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(length = 10000)
    private String content;

    @Temporal(TemporalType.DATE)
    private Date creationTime;

    private String author;

    private int nRating;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="favorite_feeds",
            joinColumns = {@JoinColumn(name = "feed_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> favoriteList = new HashSet<>();

    @Lob
    private byte[] image;

    public Feed() {
    }

    public Feed(FeedDetails feedDetails) {
        this.title = feedDetails.getTitle();
        this.content = feedDetails.getContent();
        this.author =feedDetails.getAuthor();
        this.creationTime = new Date();
        this.image = feedDetails.getImage();
        this.nRating = 0;
    }

    public int getnRating() {
        return nRating;
    }

    public void setnRating(int nRating) {
        this.nRating = nRating;
    }

    public Set<User> getFavoriteList() {
        return favoriteList;
    }

    public void setFavoriteList(Set<User> favoriteList) {
        this.favoriteList = favoriteList;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

}