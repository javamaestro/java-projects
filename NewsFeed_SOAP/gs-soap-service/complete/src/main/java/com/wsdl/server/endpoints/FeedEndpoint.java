package com.wsdl.server.endpoints;

import com.wsdl.server.model.Feed;
import com.wsdl.server.model.User;
import com.wsdl.server.repos.FeedRepository;
import com.wsdl.server.repos.UserRepository;
import com.wsdl.server.service.TypeConverter;
import io.spring.guides.gs_producing_web_service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.datatype.DatatypeConfigurationException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;


@Endpoint
public class FeedEndpoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";
    private final FeedRepository feedRepository;
    private final TypeConverter typeConverter;
    private final UserRepository userRepository;

    @Autowired
    public FeedEndpoint(FeedRepository feedRepository, TypeConverter typeConverter, UserRepository userRepository) {
        this.feedRepository = feedRepository;
        this.typeConverter = typeConverter;
        this.userRepository = userRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllFeedRequest")
    @ResponsePayload
    public GetAllFeedResponse getAllFeedResponse(@RequestPayload GetAllFeedRequest request) throws Exception {
        List<Feed> feeds = feedRepository.findAll();
        GetAllFeedResponse response = new GetAllFeedResponse();
        FeedList feedList = new FeedList();
        feedList.getFeeds().addAll(typeConverter.convertToDetails(feeds));
        response.setFeedList(feedList);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "setFeedRequest")
    @ResponsePayload
    public void setFeedResponse(@RequestPayload SetFeedRequest request) {
        SetFeedResponse response = new SetFeedResponse();
        if (request != null) {
            Feed feed = new Feed(request.getFeedDetails());
            feedRepository.save(feed);
        }
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteFeedRequest")
    @ResponsePayload
    public void deleteFeedResponse(@RequestPayload DeleteFeedRequest request) {
        if (request != null) {
            feedRepository.deleteById(request.getFeedId());
        }
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addToFavoriteRequest")
    @ResponsePayload
    public void addToFavorite(@RequestPayload AddToFavoriteRequest request) {
        User u = new User(UUID.fromString(request.getUserId()));
        userRepository.save(u);
        Feed feed = feedRepository.findById(request.getFeedId()).get();
        feed.getFavoriteList().add(u);
        feed.setnRating(feed.getnRating() + 1);
        feedRepository.save(feed);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "delFromFavoriteRequest")
    @ResponsePayload
    public void delFromFavorite(@RequestPayload DelFromFavoriteRequest request) {
        User user = userRepository.findById(UUID.fromString(request.getUserId())).get();
        Feed feed = feedRepository.findById(request.getFeedId()).get();
        feed.getFavoriteList().remove(user);
        feed.setnRating(feed.getnRating() - 1);
        feedRepository.save(feed);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllFavoriteNewsRequest")
    @ResponsePayload
    public GetAllFavoriteNewsResponse getAllFavoriteNewsRequest(@RequestPayload GetAllFavoriteNewsRequest request) throws DatatypeConfigurationException {
        User user = userRepository.findById(UUID.fromString(request.getUserId())).get();
        List<Feed> feeds = feedRepository.findAll();
        List<Feed> favFeeds = new LinkedList<>();
        FeedList favFeedList = new FeedList();
        for (Feed f : feeds) {
            if (f.getFavoriteList().contains(user)) {
                favFeeds.add(f);
            }
        }
        favFeedList.getFeeds().addAll(typeConverter.convertToDetails(favFeeds));
        GetAllFavoriteNewsResponse response = new GetAllFavoriteNewsResponse();
        response.setFavoriteFeedList(favFeedList);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "editFeedRequest")
    @ResponsePayload
    public void editFeed(@RequestPayload EditFeedRequest request) {
        Feed feed = feedRepository.findById(request.getFeedId()).get();
        feed.setTitle(request.getFeedDetails().getTitle());
        feed.setContent(request.getFeedDetails().getContent());
        feedRepository.save(feed);
    }
}
