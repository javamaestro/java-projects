package com.wsdl.server.model;

import io.spring.guides.gs_producing_web_service.CommentDetails;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "feed_comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String text;

    @Basic
    @Column(name = "feed_id", nullable = false)
    private Long feedId;

    private String author;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "comments_likes",
            joinColumns = {@JoinColumn(name = "feed_comment_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> likes = new HashSet<>();


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "comments_dislikes",
            joinColumns = {@JoinColumn(name = "feed_comment_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> dislikes = new HashSet<>();


    private int rating;

    public Comment(CommentDetails commentDetails) {
        this.id = commentDetails.getId();
        this.text = commentDetails.getText();
        this.feedId = commentDetails.getFeedId();
        this.author = commentDetails.getAuthor();
    }

    public Comment() {
    }


    public Set<User> getLikes() {
        return likes;
    }

    public void setLikes(Set<User> likes) {
        this.likes = likes;
    }

    public Set<User> getDislikes() {
        return dislikes;
    }

    public void setDislikes(Set<User> dislikes) {
        this.dislikes = dislikes;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getFeedId() {
        return feedId;
    }

    public void setFeedId(Long feedId) {
        this.feedId = feedId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


}
