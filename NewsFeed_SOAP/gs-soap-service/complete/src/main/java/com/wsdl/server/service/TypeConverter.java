package com.wsdl.server.service;

import com.wsdl.server.model.Comment;
import com.wsdl.server.model.Feed;
import com.wsdl.server.model.User;
import io.spring.guides.gs_producing_web_service.*;
import org.springframework.stereotype.Service;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;

@Service
public class TypeConverter {
    public List<FeedDetails> convertToDetails(List<Feed> feeds) throws DatatypeConfigurationException {
        List<FeedDetails> feedDetails = new ArrayList<>();
        GregorianCalendar c = new GregorianCalendar();
        for (Feed f : feeds) {
            FeedDetails fd = new FeedDetails();
            FavoriteList favoriteList = new FavoriteList();
            fd.setId(f.getId());
            fd.setTitle(f.getTitle());
            fd.setContent(f.getContent());
            fd.setAuthor(f.getAuthor());
            fd.setImage(f.getImage());
            favoriteList.getFavID().addAll(convertFromSetUserToListString(f.getFavoriteList()));
            fd.setFavList(favoriteList);
            fd.setNRating(f.getnRating());
            c.setTime(new Date());
            XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            fd.setCreationTime(date2);
            feedDetails.add(fd);
        }
        return feedDetails;
    }


    public List<String> convertFromSetUserToListString(Set<User> users) {
        List<String> userIds = new LinkedList<>();
        for (User u : users) {
            userIds.add(u.getId().toString());
        }

        return userIds;
    }


    public List<CommentDetails> convertComments(List<Comment> comments) {
        List<CommentDetails> cd = new ArrayList<>();
        for (Comment c : comments) {
            CommentDetails commentDetails = new CommentDetails();
            CommentLikes commentLikes = new CommentLikes();
            CommentDisLikes commentDisLikes = new CommentDisLikes();
            commentDetails.setAuthor(c.getAuthor());
            commentDetails.setId(c.getId());
            commentDetails.setText(c.getText());
            commentDetails.setFeedId(c.getFeedId());
            commentLikes.getUserId().addAll(convertFromSetUserToListString(c.getLikes()));
            commentDisLikes.getUserId().addAll(convertFromSetUserToListString(c.getDislikes()));
            commentDetails.setLikes(commentLikes);
            commentDetails.setDislikes(commentDisLikes);
            commentDetails.setRating(c.getRating());
            cd.add(commentDetails);
        }
        return cd;
    }
}
