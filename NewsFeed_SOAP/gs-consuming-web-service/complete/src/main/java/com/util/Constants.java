package com.util;



import org.springframework.http.MediaType;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class Constants {

   
    public static final String ROLE_REGISTERED_USER = "ROLE_REGISTERED_USER";

    public static final String ROLE_ADMINISTRATOR = "ROLE_ADMINISTRATOR";

}
