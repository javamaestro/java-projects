package com.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.util.Set;

public class UserDataResponseDTO implements JsonResponce {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private UserResponseDTO user;

    @JsonInclude()
    private OAuth2AccessToken token;

    @JsonInclude()
    private boolean isEmailVerified = true;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<String> roles;

    public UserResponseDTO getUser() {
        return user;
    }

    public void setUser(UserResponseDTO user) {
        this.user = user;
    }

    public OAuth2AccessToken getToken() {
        return token;
    }

    public void setToken(OAuth2AccessToken token) {
        this.token = token;
    }

    public boolean getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        isEmailVerified = emailVerified;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
