
package com.service.impl;

import com.dto.FeedDTO;
import com.model.UserPrincipal;
import com.service.FeedService;
import hello.wsdl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

@Service
public class FeedServiceImpl extends WebServiceGatewaySupport implements FeedService {

    private static final Logger log = LoggerFactory.getLogger(FeedServiceImpl.class);


    @Override
    public SetFeedResponse setFeed(FeedDTO feedDTO, String fullName, MultipartFile file) throws IOException {
        SetFeedRequest request = new SetFeedRequest();
        FeedDetails fd = new FeedDetails();
        fd.setTitle(feedDTO.getTitle());
        fd.setContent(feedDTO.getContent());
        fd.setAuthor(fullName);
        if (file != null && !file.isEmpty() && file.getContentType() != null) {
            try {
                byte[] byteObjects = new byte[file.getBytes().length];

                int i = 0;

                for (byte b : file.getBytes()) {
                    byteObjects[i++] = b;
                }
                fd.setImage(byteObjects);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        request.setFeedDetails(fd);
        SetFeedResponse response = (SetFeedResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/getAllCommentsRequest"));
        return response;
    }

    @Override
    public DeleteFeedRequest deleteFeed(Long feedId) {
        DeleteFeedRequest request = new DeleteFeedRequest();
        request.setFeedId(feedId);
        getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/deleteFeedRequest"));
        return request;
    }

    @Override
    public void addToFavorites(Long feedId, Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        AddToFavoriteRequest request = new AddToFavoriteRequest();
        request.setFeedId(feedId);
        request.setUserId(userPrincipal.getUser().getId().toString());
        getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/addToFavoriteRequest"));
    }

    @Override
    public void deleteFromFavorites(Long feedId, Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        DelFromFavoriteRequest request = new DelFromFavoriteRequest();
        request.setFeedId(feedId);
        request.setUserId(userPrincipal.getUser().getId().toString());
        getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/delFromFavoriteRequest"));

    }

    @Override
    public GetAllFavoriteNewsResponse getFavoriteNews(Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        GetAllFavoriteNewsRequest request = new GetAllFavoriteNewsRequest();
        request.setUserId(String.valueOf(userPrincipal.getUser().getId()));
        GetAllFavoriteNewsResponse response = (GetAllFavoriteNewsResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/getAllFavoriteNewsRequest"));
        return response;
    }

    @Override
    public FindFeedByDateResponse findAllByCreationTime(String dateTime) throws ParseException, DatatypeConfigurationException {


        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(Date.valueOf( dateTime.substring(0,10)));
        XMLGregorianCalendar xmlGregCal =  DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        FindFeedByDateRequest request = new FindFeedByDateRequest();
        request.setDate(xmlGregCal);
        FindFeedByDateResponse response = (FindFeedByDateResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/findFeedByDateRequest"));
        return response;
    }

    @Override
    public void editFeed(Authentication authentication, FeedDTO feedDTO, Long feedId) {
        EditFeedRequest request = new EditFeedRequest();
        FeedDetails feedDetails = new FeedDetails();
        feedDetails.setContent(feedDTO.getContent());
        feedDetails.setTitle(feedDTO.getTitle());
        request.setFeedDetails(feedDetails);
        request.setFeedId(feedId);
        FindFeedByDateResponse response = (FindFeedByDateResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/editFeedRequest"));
    }

    @Override
    public GetAllFeedResponse feedResponse() {
        GetAllFeedRequest request = new GetAllFeedRequest();
        request.setFlag(true);
        GetAllFeedResponse response = (GetAllFeedResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetAllFeedRequest"));

        return response;


    }

}
