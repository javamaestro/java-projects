package com.service;

import com.dto.LoginDTO;
import com.dto.NewUserDTO;
import com.dto.UserDataResponseDTO;

import java.util.List;
import java.util.UUID;

public interface UserService {

    UserDataResponseDTO registerNewUser(NewUserDTO newUserDTO);

    UserDataResponseDTO loginWithEmail(LoginDTO loginDto);

    void banUser(UUID userId, int flag);

    void doAdmin(UUID userId, int flag);

    void deleteUser(UUID userId);

    List<UserDataResponseDTO> getAllUsers();

}
