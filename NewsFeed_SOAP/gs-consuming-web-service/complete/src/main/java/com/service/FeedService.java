package com.service;

import com.dto.FeedDTO;
import hello.wsdl.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;

public interface FeedService {
    GetAllFeedResponse feedResponse();

    SetFeedResponse setFeed(FeedDTO feedDTO, String fullName, MultipartFile file) throws IOException;

    DeleteFeedRequest deleteFeed(Long feedId);

    void addToFavorites(Long feedId, Authentication authentication);

    void deleteFromFavorites(Long valueOf, Authentication authentication);


    GetAllFavoriteNewsResponse getFavoriteNews(Authentication authentication);

    FindFeedByDateResponse findAllByCreationTime(String date) throws ParseException, DatatypeConfigurationException;

    void editFeed(Authentication authentication, FeedDTO feedDTO, Long feedId);
}
