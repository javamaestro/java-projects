package com.service.impl;

import com.dto.LoginDTO;
import com.dto.NewUserDTO;
import com.dto.UserDataResponseDTO;
import com.dto.UserResponseDTO;
import com.exception.ErrorCode;
import com.exception.PrymeTimeException;
import com.model.Role;
import com.model.User;
import com.model.UserPrincipal;
import com.repositories.RoleRepository;
import com.repositories.UserRepository;
import com.service.UserService;
import com.util.Constants;
import com.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class UserServiceImpl implements UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final CustomTokenService customTokenService;

    @Autowired
    public UserServiceImpl(PasswordEncoder passwordEncoder, UserRepository userRepository, RoleRepository roleRepository, CustomTokenService customTokenService) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.customTokenService = customTokenService;
    }


    @Override
    public UserDataResponseDTO registerNewUser(NewUserDTO newUserDTO) {
        List<User> users = userRepository.findAll();
        Optional<User> userOptional = userRepository.findByEmail(newUserDTO.getEmail());
        User user;
        if (users.size() !=0 && userOptional.isPresent()) {
            user = userOptional.get();
            if (user.isEmailVerified()) {
                throw new PrymeTimeException(ErrorCode.USER_ALREADY_EXISTS, "User already exists " + newUserDTO.getEmail());
            }
        } else {
            user = new User();
        }
        user.setFullName(newUserDTO.getFullName());
        user.setEmail(newUserDTO.getEmail());
        user.setPassword(passwordEncoder.encode(newUserDTO.getPassword()));
        user.setDateJoined(DateTimeUtil.getLocalDateTimeUtc());

        user.getRoles().add(roleRepository.findByName(Constants.ROLE_ADMINISTRATOR));
        user.setEmailVerified(true);
        user.setAttemptsCount(0);
        user.setBanned(false);

        String verificationCode = generateRandomNumericString(Constants.VERIFICATION_CODE_LENGTH);
        user.setVerificationCode(verificationCode);

        userRepository.save(user);




        UserDataResponseDTO userDataResponseDto = new UserDataResponseDTO();
        userDataResponseDto.setUser(new UserResponseDTO(user));
        userDataResponseDto.setEmailVerified(true);


        return userDataResponseDto;
    }

    @Override
    public UserDataResponseDTO loginWithEmail(LoginDTO loginDto) {

        Authentication authentication = customTokenService.getAuthentication(loginDto.getEmail(), loginDto.getPassword());
        User user = ((UserPrincipal) authentication.getPrincipal()).getUser();
        UserResponseDTO userResponseDto = new UserResponseDTO(user);
        UserDataResponseDTO userDataResponseDto = new UserDataResponseDTO();
        userDataResponseDto.setUser(userResponseDto);

//      If email is not verified, then return users and isEmailVerified-flag(false)
        if (!user.isEmailVerified()) {
            userDataResponseDto.setEmailVerified(false);
            return userDataResponseDto;
        }

        OAuth2AccessToken accessToken = customTokenService.getToken(userResponseDto.getEmail());
        userDataResponseDto.setToken(accessToken);
        userDataResponseDto.setRoles(user.getRoles().stream().map(Role::getName).collect(Collectors.toSet()));

        return userDataResponseDto;
    }

    @Override
    public void banUser(UUID userId, int flag) {
        Optional<User> bannedUser = userRepository.findById(userId);
        User user = bannedUser.get();
        if (flag == 1) {
            user.setBanned(true);
        } else if (flag == 0) {
            user.setBanned(false);
        }
        userRepository.save(user);

    }

    @Override
    public void doAdmin(UUID userId, int flag) {
        Optional<User> bannedUser = userRepository.findById(userId);
        User user = bannedUser.get();
        if (flag == 1) {
            user.getRoles().add(roleRepository.findByName(Constants.ROLE_ADMINISTRATOR));
        } else if (flag == 0) {
            user.getRoles().remove(roleRepository.findByName(Constants.ROLE_ADMINISTRATOR));
        }
        userRepository.save(user);


    }

    @Override
    public void deleteUser(UUID userId) {
        userRepository.deleteById(userId);

    }

    @Override
    public List<UserDataResponseDTO> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserDataResponseDTO> urdto = new ArrayList<>();
        for (User u : users) {
            UserDataResponseDTO userDataResponseDto = new UserDataResponseDTO();
            userDataResponseDto.setUser(new UserResponseDTO(u));
            urdto.add(userDataResponseDto);
        }
        return urdto;
    }


}
