package com.service;

import com.dto.CommentDTO;
import hello.wsdl.GetAllCommentsResponse;
import hello.wsdl.SetCommentResponse;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

public interface CommentService {
    GetAllCommentsResponse getComments(Long feedId);

    SetCommentResponse addComment(CommentDTO commentDTO, String feedId, Authentication authentication, MultipartFile file);

    void deleteComment(Long valueOf);

    void changeRating(Long valueOf, String flag, Authentication authentication);

    void returnRating(Long valueOf, String flag, Authentication authentication);
}
