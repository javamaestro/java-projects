package com.service.impl;

import com.dto.CommentDTO;
import com.model.UserPrincipal;
import com.service.CommentService;
import hello.wsdl.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.io.IOException;

@Service
public class CommentServiceImpl extends WebServiceGatewaySupport implements CommentService {
    @Override
    public GetAllCommentsResponse getComments(Long feedId) {
        GetAllCommentRequest request = new GetAllCommentRequest();
        request.setFeedId(feedId);
        GetAllCommentsResponse response = (GetAllCommentsResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/getAllCommentsResponse"));

        return response;
    }

    @Override
    public SetCommentResponse addComment(CommentDTO commentDTO, String feedId, Authentication authentication, MultipartFile file) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        SetCommentRequest request = new SetCommentRequest();
        CommentDetails commentDetails = new CommentDetails();
        commentDetails.setText(commentDTO.getText());
        commentDetails.setAuthor(userPrincipal.getUser().getFullName());
        commentDetails.setFeedId(Long.valueOf(feedId));
        if (file != null && !file.isEmpty() && file.getContentType() != null) {
            try {
                byte[] byteObjects = new byte[file.getBytes().length];

                int i = 0;

                for (byte b : file.getBytes()) {
                    byteObjects[i++] = b;
                }
                commentDetails.setImage(byteObjects);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        request.setComment(commentDetails);
        SetCommentResponse response = (SetCommentResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/setCommentRequest"));
        return response;
    }

    @Override
    public void deleteComment(Long commentId) {
        DeleteCommentRequest request = new DeleteCommentRequest();
        request.setCommentId(Long.valueOf(commentId));
        getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/deleteCommentRequest"));
    }

    @Override
    public void changeRating(Long commentId, String flag, Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        ChangeRatingRequest request = new ChangeRatingRequest();
        request.setCommentId(commentId);
        request.setFlag(flag);
        request.setUserID(String.valueOf(userPrincipal.getUser().getId()));
        getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/changeRatingRequest"));

    }

    @Override
    public void returnRating(Long commentId, String flag, Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        ReturnRatingRequest request = new ReturnRatingRequest();
        request.setCommentId(commentId);
        request.setFlag(flag);
        request.setUserID(String.valueOf(userPrincipal.getUser().getId()));
        getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8888/ws/feeds", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/returnRatingRequest"));

    }
}
