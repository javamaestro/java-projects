package com.controller;

import com.dto.FeedDTO;
import com.model.UserPrincipal;
import com.service.FeedService;
import com.util.Util;
import hello.wsdl.FeedDetails;
import hello.wsdl.FeedList;
import hello.wsdl.GetAllFeedResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.xml.datatype.DatatypeConfigurationException;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/feeds")
public class FeedController {

    private final FeedService feedService;

    @Autowired
    public FeedController(FeedService feedService ) {
        this.feedService = feedService;
    }

    @GetMapping("/newsFeed")
    public ResponseEntity<Object> mainPage(@RequestParam(value = "page", defaultValue = "0") int page,
                                           @RequestParam(value = "size", defaultValue = "10") int size,
                                           Pageable pageable) {
        List<FeedDetails> feeds = feedService.feedResponse().getFeedList().getFeeds();
        PagedListHolder<FeedDetails> feedPage = new PagedListHolder<>(feeds);
        feedPage.setPage(page);
        feedPage.setPageSize(size);
        return Util.createResponseEntity(feedPage);
    }

    @PostMapping(value = "/addFeed", produces = "application/json", consumes = "multipart/form-data")
    public ResponseEntity<Object> addFeed(
            Authentication authentication,
            @Valid @ModelAttribute FeedDTO feedDTO,
            @RequestParam(required = false, name = "file") MultipartFile file) throws IOException {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        feedService.setFeed(feedDTO, userPrincipal.getUser().getFullName(), file);
        return Util.createResponseEntity(Collections.EMPTY_MAP);
    }

    @GetMapping(value = "/delete/{feedId}", produces = "application/json")
    public ResponseEntity<Object> deleteFeed(@PathVariable("feedId") String feedId) {
        feedService.deleteFeed(Long.valueOf(feedId));
        return Util.createResponseEntity("OK!");
    }

    @GetMapping("/favoriteNews/add/{feedId}")
    public ResponseEntity<Object> addToFavorites(
            @PathVariable("feedId") String feedId,
            Authentication authentication){
        feedService.addToFavorites(Long.valueOf(feedId), authentication);
        return Util.createResponseEntity(Collections.EMPTY_MAP);
    }

    @DeleteMapping("/favoriteNews/delete/{feedId}")
    public ResponseEntity<Object> deleteFavoriteNew(
            Authentication authentication,
            @PathVariable("feedId") String feedId
    ){
        feedService.deleteFromFavorites(Long.valueOf(feedId), authentication);
        return Util.createResponseEntity(Collections.EMPTY_MAP);
    }

    @GetMapping("/favoriteNews/getAll")
    public ResponseEntity<Object> favoriteNews(
            Authentication authentication){
        return Util.createResponseEntity(feedService.getFavoriteNews(authentication));
    }

    @GetMapping("/newsFeedByDate")
    public ResponseEntity<Object> newsByDate(@RequestParam(value = "date") String date,
                                             @RequestParam(value = "page", defaultValue = "0") int page,
                                             @RequestParam(value = "size", defaultValue = "10") int size,
                                             Authentication authentication) throws ParseException, DatatypeConfigurationException {
        System.out.println(date.substring(0,10));

        List<FeedDetails> feeds = feedService.findAllByCreationTime(date).getFeedList().getFeeds();
        Page<FeedDetails> feedPage = new PageImpl<>(feeds, new PageRequest(page, size), feeds.size());

        return Util.createResponseEntity(feedPage);
    }

    @PostMapping(value = "/editFeed")
    public ResponseEntity<Object> editFeed(
            Authentication authentication,
            @Valid @ModelAttribute FeedDTO feedDTO,
            @RequestParam (name = "feedId") Long feedId
    ){
        feedService.editFeed(authentication, feedDTO, feedId);
        return Util.createResponseEntity("OKEY!");
    }




}
