package com.controller;


import com.dto.LoginDTO;
import com.dto.NewUserDTO;
import com.dto.UserDataResponseDTO;
import com.repositories.UserRepository;
import com.service.UserService;
import com.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/users")
class UserController {


    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;


    @PostMapping(value = "/registerWithEmail", produces = "application/json")
    public ResponseEntity<Object> registerUser(@Valid @RequestBody NewUserDTO newUserDTO) {
        return Util.createResponseEntity(userService.registerNewUser(newUserDTO));
    }

    @PostMapping(value = "/loginWithEmail", produces = "application/json")
    public ResponseEntity<Object> loginWithEmail(@Valid @RequestBody LoginDTO loginDto) {
        return Util.createResponseEntity(userService.loginWithEmail(loginDto));
    }

    @GetMapping("/getAllUsers")
    public List<UserDataResponseDTO> getAllUsers() {
        return userService.getAllUsers();
    }


    @GetMapping("/ban/{userId}")
    public ResponseEntity<Object> banUser(
            Authentication authentication,
            @PathVariable(value = "userId") String userId,
            @RequestParam(value = "flag") String flag
    ) {
        userService.banUser(UUID.fromString(userId), Integer.valueOf(flag));
        return Util.createResponseEntity(Collections.EMPTY_MAP);
    }

    @GetMapping("/doAdmin/{userId}")
    public ResponseEntity<Object> doAdmin(
            Authentication authentication,
            @PathVariable(value = "userId") String userId,
            @RequestParam(value = "flag") String flag
    ) {
        userService.doAdmin(UUID.fromString(userId), Integer.valueOf(flag));
        return Util.createResponseEntity(Collections.EMPTY_MAP);
    }

    @DeleteMapping("/deleteUser/{userId}")
    public ResponseEntity<Object> deleteUser(
            Authentication authentication,
            @PathVariable(value = "userId") String userId
    ){
        userService.deleteUser(UUID.fromString(userId));
        return  Util.createResponseEntity(Collections.EMPTY_MAP);

    }


}
