package com.controller;

import com.dto.CommentDTO;
import com.service.CommentService;
import com.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Collections;

@RestController
@RequestMapping("comments")
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/{feedId}")
    public ResponseEntity<Object> getComments(@PathVariable("feedId") String feedId){
        return Util.createResponseEntity(commentService.getComments(Long.valueOf(feedId)));

    }
    @PostMapping(value = "/{feedId}", produces = "application/json", consumes = "multipart/form-data")
    public ResponseEntity<Object> addComment(
            Authentication authentication,
            @Valid @ModelAttribute CommentDTO commentDTO,
            @RequestParam(required = false, name = "file") MultipartFile file,
            @PathVariable("feedId") String feedId)
    {
        commentService.addComment(commentDTO, feedId, authentication, file);
        return Util.createResponseEntity("OK!");
    }
    @DeleteMapping("/{commentId}")
    public ResponseEntity<Object> deleteComment(
            Authentication authentication,
            @PathVariable("commentId") String commentId){
            commentService.deleteComment(Long.valueOf(commentId));
            return Util.createResponseEntity(Collections.EMPTY_MAP);
    }

    @GetMapping("/changeRating/{commentId}")
    public ResponseEntity<Object> changeRating(
            Authentication authentication,
            @PathVariable("commentId") String commentId,
            @RequestParam(value = "flag") String flag){
        commentService.changeRating(Long.valueOf(commentId), flag, authentication);
        return Util.createResponseEntity(Collections.EMPTY_MAP);
    }

    @GetMapping("/returnRating/{commentId}")
    public  ResponseEntity<Object> returnRating(
            Authentication authentication,
            @PathVariable("commentId") String commentId,
            @RequestParam(value = "flag") String flag){
        commentService.returnRating(Long.valueOf(commentId), flag, authentication);
        return Util.createResponseEntity(Collections.EMPTY_MAP);

    }
}
