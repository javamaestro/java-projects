
package com.config;

import com.service.impl.CommentServiceImpl;
import com.service.impl.FeedServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class FeedConfiguration {

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		// this package must match the package in the <generatePackage> specified in
		// pom.xml
		marshaller.setContextPath("hello.wsdl");
		return marshaller;
	}

	@Bean
	public FeedServiceImpl feedService(Jaxb2Marshaller marshaller) {
		FeedServiceImpl client = new FeedServiceImpl();
		client.setDefaultUri("http://localhost:8888/ws");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

	@Bean
	public CommentServiceImpl commentService (Jaxb2Marshaller marshaller) {
		CommentServiceImpl client = new CommentServiceImpl();
		client.setDefaultUri("http://localhost:8888/ws");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}


}
