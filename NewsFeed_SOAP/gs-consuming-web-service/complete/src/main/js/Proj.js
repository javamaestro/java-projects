import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect, Link, withRouter } from 'react-router-dom';
import { SideMenu, Item } from 'react-sidemenu';
import News from './components/News';
import Register from './components/Register';
import UserList from './components/Users';
import Verification from './components/Verification';
import Login from './components/Login';
import Favorites from './components/Favorites';
import Video from './components/video';
import axios from 'axios';


export default class MainPage extends Component {
  state = {
    startDate: new Date()
  };

  handleChange = date => {
    this.setState({
      startDate: date
    });
  };

  render() {
    Auth.authenticate();
    RoleChecking.checkRole();
    return (
      <div class="full-background">
        <header>
          <div class="bg">
            <h1 align="center">Welcome to News Feed!</h1>
          </div>
        </header>
        <Router>
          <div class="container-fluid">
            <div class="row">
              <div class="col-sm">

                <div>
                  <ul class="nav nav-tabs">
                    <li class="list-inline-item"><Link to={'/news'} className="nav-link">Feeds</Link></li>
                    <li class="list-inline-item"><Link to={'/favorites'} className="nav-link">Favorites</Link></li>
                    {RoleChecking.adminRole ? (<li class="list-inline-item"><Link to={'/users'} className="nav-link">Users</Link></li>) : (<></>)}
                  </ul>
                  <Switch>
                    <Route path='/register' component={Register} />
                    <Route path='/verification' component={Verification} />
                    <Route path='/favorites' component={Favorites} />
                    <Route path='/video' component={Video} />
                    <PrivateRoute path='/news' component={News} />
                    {RoleChecking.adminRole ? (<Route path='/users' component={UserList} />
                    ) : (<></>)}
                  </Switch>
                </div>
              </div>
              {Auth.isAuthenticated ? (
                <div class="col-md-auto">
                  Hello, {localStorage.getItem("fullName")}!
              <AuthButton />
                </div>) :
                (<div class="col-md-auto">
                  Authorization:
               <Route path='/' component={Login} />
                </div>)}
            </div>
          </div>
        </Router>
      </div >
    );
  }
}




const AuthButton = (
  ({ history }) =>
    Auth.isAuthenticated ? (
      <p>
        <button type="button" class="btn btn-danger"
          onClick={() => {
            Auth.signout(() => history.push("/"));
          }}
        >
          Sign out
        </button>
      </p>
    ) : (
        <p>You are not logged in.</p>
      )
);



function PrivateRoute({ component: Component, ...rest }) {
  Auth.authenticate();
  return (
    <Route
      {...rest}
      render={props =>
        Auth.isAuthenticated ? (
          <Component {...props} />
        ) : (
            <></>
          )
      }
    />
  );
}

const RoleChecking = {
  adminRole: false,
  checkRole() {
    if (localStorage.getItem('roles') == 'ROLE_ADMINISTRATOR') {
      this.adminRole = true;
    }

  }
}

const Auth = {
  isAuthenticated: false,
  authenticate() {
    if (localStorage.getItem('access_token')) {
      this.isAuthenticated = true;
    }
  },

  signout() {
    localStorage.clear();
    this.isAuthenticated = false;
    window.location.href = "/";
  }
}

