import React from 'react';
import ReactDOM from 'react-dom';
import axios, * as others from 'axios';


export default class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      fullName: '',
      email: '',
      users: []
    };
    this.banUser = this.banUser.bind(this);
    this.handleAdminRole = this.handleAdminRole.bind(this);
    this.handleDeleteUser = this.handleDeleteUser.bind(this);
  }

  handleDeleteUser(user) {
    axios.delete('http://localhost:8080/users/deleteUser/' + user.userId)
      .then((response) => {
        this.loadFromServer();
      });
  }

  handleAdminRole(user, flag) {

    axios.get('http://localhost:8080/users/doAdmin/' + user.userId, {
      params: { 'flag': flag },

      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    })
      .then((response) => {
        this.loadFromServer();
      });
  }



  banUser(user, flag) {

    axios.get('http://localhost:8080/users/ban/' + user.userId, {
      params: { 'flag': flag },

      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    })
      .then((response) => {
        this.loadFromServer();
      });
  }

  loadFromServer() {
    axios.get('http://localhost:8080/users/getAllUsers', {
      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    })
      .then((response) => {
        this.setState({
          users: response.data
        });
      }).catch((error) => {

        if (error.response.status == 401)
          axios.get('http://localhost:8080/oauth/token',
            {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': 'bearer' + localStorage.getItem("access_token"),
            },
            {
              'client_id': 'prymetime-client-id',
              'client_secret': 'prymetime-client-secret',
              'grant_type': 'refresh_token',
              'refresh_token': localStorage.getItem('refresh_token'),
              'scope': 'prymetime'
            }

          ).then((response) => {
          }
          );
        this.setState({ redirecting: false });
      });
  }

  componentDidMount() {
    this.loadFromServer();
  }

  render() {
    return (
      <div>
        <label>User list: </label>
        <UserList users={this.state.users} banUser={this.banUser} handleAdminRole={this.handleAdminRole} handleDeleteUser={this.handleDeleteUser} />
      </div>
    )
  }

}


class UserList extends React.Component {
  render() {
    const users = this.props.users.map((user) =>
      <User key={user.id} user={user} banUser={this.props.banUser} handleAdminRole={this.props.handleAdminRole} handleDeleteUser={this.props.handleDeleteUser} />);
    return (
      <table>
        <tbody>
          <tr>
            <th>Email</th>
            <th>Full Name</th>
            <th>Role</th>
            <th>Ban user</th>
            <th>is Admin</th>
            <th></th>
          </tr>
          {users}
        </tbody>
      </table>
    )
  }
}


class User extends React.Component {
  constructor(props) {
    super(props);
    this.handleUserBan = this.handleUserBan.bind(this);
    this.handleUserAdmin = this.handleUserAdmin.bind(this);
    this.handleDelete = this.handleDelete.bind(this);

  }


  handleDelete() {
    var user = this.props.user.user;
    this.props.handleDeleteUser(user);
  }

  handleUserBan() {
    var isBanned = this.props.user.user.banned;
    var isBannedFlag = "";
    if (isBanned == false) {
      isBannedFlag = "1";
    } else if (isBanned == true) {
      isBannedFlag = "0";
    }
    this.props.banUser(this.props.user.user, isBannedFlag);
  }

  handleUserAdmin() {
    var isAdmin = handleCheckRole(this.props.user.user.roles);
    var isAdminFlag = "";
    if (isAdmin == false) {
      isAdminFlag = "1";
    } else if (isAdmin == true) {
      isAdminFlag = "0";
    }
    this.props.handleAdminRole(this.props.user.user, isAdminFlag);
  }

  render() {
    return (
      <tr>
        <td>{this.props.user.user.email}</td>
        <td>{this.props.user.user.fullName}</td>
        <td>{this.props.user.user.roles.map((role) => <p>{role.name}</p>)}</td>
        <td><input type="checkbox" defaultChecked={this.props.user.user.banned} onChange={this.handleUserBan} name="isbaned" /></td>
        <td><input type="checkbox" defaultChecked={handleCheckRole(this.props.user.user.roles)} onChange={this.handleUserAdmin} name="isAdmin" /></td>
        <button class="btn btn-danger" onClick={this.handleDelete}>Delete</button>
      </tr>
    )
  }
}

function handleCheckRole(roles) {
  var rolesL = roles.length;
  for (var i = 0; i < rolesL; i++) {
    if (roles[i]["name"] == "ROLE_ADMINISTRATOR") {
      return true;
    }
  }
  return false;
}
