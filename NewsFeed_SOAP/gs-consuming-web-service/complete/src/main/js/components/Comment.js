import React, { Component } from 'react';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import axios, * as others from 'axios';

class Comm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commentContent: '',
      author: '',
      feed: '',
      comments: [],
      isAdmin: false,
      selectedFile: null,
    };
    this.toggle = this.toggle.bind(this);
    this.state = { collapse: false};
    this.state = { arrowOpen: false};
    this.state = { className: "fa fa-arrow-down"  };
    this.handleCommentSender = this.handleCommentSender.bind(this);
    this.handleComment = this.handleComment.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleLoadComments = this.handleLoadComments.bind(this);
    this.commDelete = this.commDelete.bind(this);
    this.changeRating = this.changeRating.bind(this);
    this.returnRating = this.returnRating.bind(this);
  }

  toggle(e) {
  var open = false;
    this.setState(state => ({ collapse: !state.collapse }));
    if(this.state.arrowOpen){
            this.setState(state => ({ className: "fa fa-arrow-down" }));
    }else {
        this.setState(state => ({ className: "fa fa-arrow-down open" }));
    }
        this.setState(state => ({ arrowOpen: !state.arrowOpen }));
  }

  handleComment(e) {
    this.setState({ commentContent: e.target.value })
  }

  handleFileChange(eve) {
    this.setState({ selectedFile: eve.target.files[0] })
  }

  returnRating(comment, flag) {
    axios.get('http://localhost:8080/comments/returnRating/' + comment.id, {
      params: { 'flag': flag },
      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    }).then((response) => {
      this.handleLoadComments();
    })
  }



  changeRating(comment, flag) {
    axios.get('http://localhost:8080/comments/changeRating/' + comment.id, {
      params: { 'flag': flag },
      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    }).then((response) => {
      this.handleLoadComments();
    })
  }

  commDelete(comment) {
    axios.delete('http://localhost:8080/comments/' + comment.id)
      .then((response) => {
        this.handleLoadComments();
      });
  }

  handleCommentSender(event) {
    event.preventDefault();
    const data = new FormData();
    data.append('text',this.state.commentContent);
    data.append('file', this.state.selectedFile);
    axios.post('http://localhost:8080/comments/' + this.props.feed.id, data,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + localStorage.getItem("access_token")

        }
      }).then((response) => {
        this.handleLoadComments();
          });
  }

  componentDidMount() {
    this.handleLoadComments();
  }

  handleLoadComments() {
    axios.get('http://localhost:8080/comments/' + this.props.feed.id, {
      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    })
      .then((response) => {
        this.setState({
          comments: response.data.response.commentList.comments
        });
      }).catch((error) => {
        console.log(error);
      })
  }

  render() {
    return (
      <div>
          <div id="container-arrow">
            <i id="icon" class={this.state.className} onClick={this.toggle} style={{ marginBottom: '1rem' }}></i>
          </div>
        <Collapse isOpen={this.state.collapse}>
          <Card>
            <CardBody>
              <form>
                <h6>Comment additionad</h6>
                <div>
                  <label>Text</label>
                  <br />
                  <textarea rows="5" cols="30" name="content" onChange={this.handleComment}></textarea>
                </div>
                <div>
                  <input type="file" class="btn btn-light" name="file" id="file" placeholder="Select a file for upload" onChange={this.handleFileChange} />
                </div>
                <div>
                  <button type="button" onClick={this.handleCommentSender}>Add</button>
                </div>
              </form>
              <div>
                {this.state.comments ? (
                  <CommentList commDelete={this.commDelete} comments={this.state.comments} changeRating={this.changeRating} returnRating={this.returnRating} />
                ) : (
                    <div>Loading comments</div>
                  )}
              </div>
            </CardBody>
          </Card>
        </Collapse>
      </div>
    );
  }
}

class CommentList extends React.Component {
  render() {
    const comments = this.props.comments.map((comment) =>
      <Comment key={comment.id} comment={comment} commDelete={this.props.commDelete} changeRating={this.props.changeRating} returnRating={this.props.returnRating} />
    );
    return (
      <div>
        <div>
          <h2>Comment List</h2>
          {comments}
        </div>
        <div>
        </div>
      </div>
    )
  }
}

class Comment extends React.Component {
  constructor(props) {
    super(props);
    this.handleCommDelete = this.handleCommDelete.bind(this);
    this.handleChangeRatingUp = this.handleChangeRatingUp.bind(this);
    this.handleChangeRatingDown = this.handleChangeRatingDown.bind(this);
    this.isLikeChecking = this.isLikeChecking.bind(this);
    this.handleReturnLike = this.handleReturnLike.bind(this);
    this.handleReturnDislike = this.handleReturnDislike.bind(this);
  }

  handleCommDelete() {
    this.props.commDelete(this.props.comment);
  }

  handleChangeRatingUp() {
    var flag = "up";
    this.props.changeRating(this.props.comment, flag)
  }

  handleChangeRatingDown() {
    var flag = "down";
    this.props.changeRating(this.props.comment, flag)
  }

  handleReturnLike() {
    var flag = "like";
    this.props.returnRating(this.props.comment, flag)
  }

  handleReturnDislike() {
    var flag = "dislike";
    this.props.returnRating(this.props.comment, flag)
  }

  isLikeChecking() {
    var likes = this.props.comment.likes.userId;
    if (likes.length == 0) {
      return false;
    }
    if (likes.length != 0) {
      for (var i = 0; i < likes.length; i++) {
        if (likes[i] == localStorage.getItem('userId')) {
          return true;
        }
      }
    }
    return false;
  }

  isDisLikeChecking() {
    var dislikes = this.props.comment.dislikes.userId;
    if (dislikes.length == 0) {
      return false;
    }
    if (dislikes.length != 0) {
      for (var i = 0; i < dislikes.length; i++) {
        if (dislikes[i] == localStorage.getItem('userId')) {
          return true;
        }
      }
    }
    return false;
  }



  render() {
    isAdminChecking.check();
    return (
      <div>
        <p>{this.props.comment.text}</p>
        <p align="right">Author: {this.props.comment.author}</p>
        {this.isLikeChecking() ? (
                  <i class="fas fa-thumbs-up" onClick={this.handleReturnLike}></i>
                ) : (<i class="far fa-thumbs-up" onClick={this.handleChangeRatingUp}></i>)
                }
                {this.props.comment.rating}
                {this.isDisLikeChecking() ? (
                  <i class="fas fa-thumbs-down" onClick={this.handleReturnDislike}></i>
                ) : (<i class="far fa-thumbs-down" onClick={this.handleChangeRatingDown}></i>)
                }
        <br />
        <hr />
      </div >
    )
  }
}

const isAdminChecking = {
  isAdmin: false,
  check() {
    if (localStorage.getItem('roles') == 'ROLE_ADMINISTRATOR') {
      this.isAdmin = true;
    }
  }
}

export default Comm;
