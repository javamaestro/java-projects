import React from 'react';
import axios from 'axios';


export default class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleFullNameChange = this.handleFullNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      email: '',
      password: '',
      fullName: ''
    };
  }

  handleEmailChange(e) {
    this.setState({ email: e.target.value })
  }
  handlePasswordChange(e) {
    this.setState({ password: e.target.value })
  }
  handleFullNameChange(e) {
    this.setState({ fullName: e.target.value })
  }

  handleSubmit(event) {
    event.preventDefault();
    axios.post('http://localhost:8080/users/registerWithEmail',
      {
        email: this.state.email,
        password: this.state.password,
        fullName: this.state.fullName
      }, {
      'Content-Type': 'application/json',
    }
    ).then((response) => {
      window.location.href = "/verification";
    });
  }


  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Person email:
                     <input type="text" name="email" onChange={this.handleEmailChange} />
          </label>
          <br />
          <label>
            Person password:
                    <input type="password" name="password" onChange={this.handlePasswordChange} />
          </label>
          <br />
          <label>
            Person fullName:
                     <input type="text" name="fullName" onChange={this.handleFullNameChange} />
          </label>
          <button>Registration</button>
        </form>
      </div>
    )
  }
}
