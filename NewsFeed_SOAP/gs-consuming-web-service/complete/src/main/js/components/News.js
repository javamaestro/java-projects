import React from 'react';
import ReactDOM from 'react-dom';
import axios, * as others from 'axios';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native-web';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import { UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';
import { BrowserRouter as Router, Switch, Route, Redirect, Link, withRouter } from 'react-router-dom';
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

import Comment from './Comment';
import { da } from 'date-fns/esm/locale';


export default class News extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      title: '',
      content: '',
      selectedFile: null,
      feeds: [],
      pageSize: '',
      totalPages: '',
      page: '',
      author: '',
      collapse: false,
      startDate: new Date(),
      editId: ''
    };
    this.toggle = this.toggle.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleContentChange = this.handleContentChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.handleNews = this.handleNews.bind(this);
    this.handleNews2 = this.handleNews2.bind(this);
    this.handleNews3 = this.handleNews3.bind(this);
    this.loadFirstPage = this.loadFirstPage.bind(this);
    this.loadLastPage = this.loadLastPage.bind(this);
    this.loadPrevPage = this.loadPrevPage.bind(this);
    this.loadNextPage = this.loadNextPage.bind(this);
    this.addToFavorite = this.addToFavorite.bind(this);
    this.returnLike = this.returnLike.bind(this);
    this.editFeed = this.editFeed.bind(this);
  }

  handleTitleChange(eee) {
    eee.preventDefault();
    this.setState({ title: eee.target.value })
  }
  handleContentChange(ev) {
    ev.preventDefault();
    this.setState({ content: ev.target.value })
  }
  handleFileChange(eve) {
    eve.preventDefault();
    this.setState({ selectedFile: eve.target.files[0] })
  }

  handleSubmit(event) {
    event.preventDefault();
    const data = new FormData();
    console.log(this.state);
    data.append('title', this.state.title);
    data.append('content', this.state.content);
    data.append('file', this.state.selectedFile);
    axios.post('http://localhost:8080/feeds/addFeed', data,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + localStorage.getItem("access_token")
        }
      }
    ).then((response) => {
      this.loadFromServer();
    });
  }


  loadFromServer(pageSize, page) {
    axios.get('http://localhost:8080/feeds/newsFeed', {
      params: {
        'size': pageSize,
        'page': page
      },
      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    })
      .then((response) => {
        console.log(response);
        this.setState({
          feeds: response.data.response.pageList,
          totalPages: response.data.response.pageCount,
        });
        window.scrollTo(0, 0);
      }).catch((error) => {
      });
  }

  onDelete(feed) {
    axios.get('http://localhost:8080/feeds/delete/' + feed.id)
      .then((response) => {
        this.loadFromServer();
      });
  }

  addToFavorite(feed) {
    axios.get('http://localhost:8080/feeds/favoriteNews/add/' + feed.id, {
      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    }).then((response) => {
      this.loadFromServer();
    })
  }

  returnLike(feed) {
    axios.delete('http://localhost:8080/feeds/favoriteNews/delete/' + feed.id, {
      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    }).then((response) => {
      this.loadFromServer();
    })
  }

  editFeed(feed, titleEdit, contentEdit) {

    const data = new FormData();
    data.append('title', titleEdit);
    data.append('content', contentEdit);
    data.append('feedId', feed);
    axios.post('http://localhost:8080/feeds/editFeed', data,
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + localStorage.getItem("access_token")
        }
      }
    ).then((response) => {
      this.loadFromServer();
    });
  }

  componentDidMount() {
    this.loadFromServer();
  }


  toggle() {
    this.setState(state => ({ collapse: !state.collapse }));
  }

  loadFirstPage() {
    var page = 0;
    var pageSize = this.state.pageSize;
    this.setState({ page: page });
    this.loadFromServer(pageSize, page);
  }


  loadPrevPage() {
    var pageSize = this.state.pageSize;
    var page = this.state.page;
    if (page > 0 && page <= this.state.totalPages - 1) {
      page -= 1;
      this.setState({ page: page });
    } else {
      this.loadFromServer(pageSize, 0);
    }
    this.loadFromServer(pageSize, page);
  }

  loadNextPage() {
    var pageSize = this.state.pageSize;
    var page = this.state.page;
    if (page < (this.state.totalPages - 1) && page >= 0) {
      page += 1;
      this.setState({ page: page });
    } else {
      page = this.state.totalPages - 1;
      this.loadFromServer(pageSize, page);
    }
    this.loadFromServer(pageSize, page);
  }

  loadLastPage() {
    var page = this.state.totalPages - 1;
    var pageSize = this.state.pageSize;
    this.setState({ page: page });
    this.loadFromServer(pageSize, page);
  }

  handleNews() {
    var pageSize = "5";
    var page = "0";
    this.setState({ pageSize: pageSize });
    this.loadFromServer(pageSize, page);
  }
  handleNews2() {
    var pageSize = "10";
    var page = "0";
    this.setState({ pageSize: pageSize });
    this.loadFromServer(pageSize, page);
  }
  handleNews3() {
    var pageSize = "15";
    var page = "0";
    this.setState({ pageSize: pageSize });
    this.loadFromServer(pageSize, page);
  }
  render() {
    return (
      <div>
        <div>
          <div class="filters">
            <label>Filter news</label>
            <View style={{ flexDirection: 'row' }}>
              <DatePicker
                selected={this.state.startDate}
                onChange={this.handleChange}
                dateFormat="yyyy-MM-dd"
              />
              <label>Sort by rating</label>
              <i class="far fa-star fa-2x" onClick={this.sortByRating}></i>
              <label>Clean filters</label>
              <i class="far fa-times-circle fa-2x" onClick={this.loadFirstPage}></i>
            </View>
          </div>
          <div>
            <label>The number of feeds per page</label>
            <br />
            <div class="btn-group" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-secondary" onClick={this.handleNews} >5</button>
              <button type="button" class="btn btn-secondary" onClick={this.handleNews2} >10</button>
              <button type="button" class="btn btn-secondary" onClick={this.handleNews3} >15</button>
            </div>
            <br />
          </div>
        </div>
        <div>
          <Button color="primary" onClick={this.toggle} style={{ marginBottom: '1rem' }}>Open news editor</Button>
          <Collapse isOpen={this.state.collapse}>
            <Card>
              <CardBody>
                <form>
                  <h2>Adding feed</h2>
                  <div >
                    <label>Title</label>
                    <br />
                    <input type="text" name="title" onChange={this.handleTitleChange} />
                  </div>
                  <div>
                    <label>Content</label>
                    <br />
                    <textarea rows="20" cols="70" name="content" onChange={this.handleContentChange}></textarea>
                  </div>
                  <div>
                    <input type="file" class="btn btn-light" name="file" id="file" placeholder="Select a file for upload" onChange={this.handleFileChange} />
                  </div>
                  <div>
                    <button class="btn btn-success" type="button" onClick={this.handleSubmit}>Add..</button>
                  </div>
                </form>
              </CardBody>
            </Card>
          </Collapse>
        </div>
        <FeedList feeds={this.state.feeds}
          onDelete={this.onDelete}
          addToFavorite={this.addToFavorite}
          returnLike={this.returnLike}
          editFeed={this.editFeed}
          editId={this.state.editId} />
        <nav aria-label="Page navigation example">
          <ul class="pagination">
            <li class="page-item"><a class="page-link" onClick={this.loadFirstPage}>1</a></li>
            <li class="page-item"><a class="page-link" onClick={this.loadPrevPage}>2</a></li>
            <li class="page-item"><a class="page-link" onClick={this.loadNextPage}>3</a></li>
            <li class="page-item"><a class="page-link" onClick={this.loadLastPage}>4</a></li>
          </ul>
        </nav>
      </div >
    )
  }
}

class FeedList extends React.Component {
  render() {
    const feeds = this.props.feeds.map((feed) =>
      <Feed key={feed.id} feed={feed} onDelete={this.props.onDelete} addToFavorite={this.props.addToFavorite} returnLike={this.props.returnLike} editFeed={this.props.editFeed} editId={this.props.editId} />
    );
    return (
      <div>
        <div class="feed">
          {feeds}
        </div>
        <div>
        </div>
      </div>
    )
  }
}

class Feed extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      titleEdit: '',
      contentEdit: '',
      fId: ''
    };
    this.handleEditTitle = this.handleEditTitle.bind(this);
    this.handleEditContent = this.handleEditContent.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.addToFavorite = this.addToFavorite.bind(this);
    this.isLiked = this.isLiked.bind(this);
    this.backLike = this.backLike.bind(this);
    this.edittFeed = this.edittFeed.bind(this);
    this.setEdittedId = this.setEdittedId.bind(this);

  }

  handleEditTitle(e) {
    this.setState({ titleEdit: e.target.value });
  }

  handleEditContent(e) {
    this.setState({ contentEdit: e.target.value });
  }

  setEdittedId(e) {
    this.setState({ fId: this.props.feed.id }, function () {
      console.log(this.state.fId);
    });

  }

  edittFeed() {
    console.log(this.state.dId);
    this.props.editFeed(this.state.fId, this.state.titleEdit, this.state.contentEdit)
  }


  handleDelete(feed) {
    this.props.onDelete(this.props.feed);
  }

  addToFavorite() {
    this.props.addToFavorite(this.props.feed)
  }

  isLiked() {
    var favorites = this.props.feed.favList.favID;
    if (favorites.length == 0) {
      return false;
    }
    if (favorites.length != 0) {
      for (var i = 0; i < favorites.length; i++) {
        if (favorites[i] == localStorage.getItem('userId')) {
          return true;
        }
      }
    }
    return false;
  }
  backLike() {

    this.props.returnLike(this.props.feed);
  }

  render() {
    var binary_data = this.props.feed.image;
    console.log(this.props.feed);
    return (
      <div class="blog-post">
        <View style={{ flexDirection: 'row' }}>
          <h3 class="ptitile">{this.props.feed.title}</h3>
          <i align="right" class="fas fa-times fa-2x" onClick={this.handleDelete}></i>
          <i class="fas fa-pencil-alt fa-2x" id="PopoverClick" onClick={this.setEdittedId}></i>
        </View>
        <UncontrolledPopover class="popover" trigger="click" placement="bottom" target="PopoverClick">
          <PopoverHeader>Edit Feed</PopoverHeader>
          <PopoverBody>
            <form>
              <h2>Editing feed</h2>
              <div >
                <label>Title</label>
                <br />
                <input type="text" name="title" onChange={this.handleEditContent} />
              </div>
              <div>
                <label>Content</label>
                <br />
                <textarea rows="10" cols="20" name="content" onChange={this.handleEditTitle}></textarea>
              </div>
              <div>
                <button class="btn btn-success" type="button" onClick={this.edittFeed}>Edit..</button>
              </div>
            </form>
          </PopoverBody>
        </UncontrolledPopover>
        <h6 align="right">{this.props.feed.creationTime}</h6>
        <h6><img src={`data:image/jpeg;base64,${binary_data}`} width="300" height="300"
          alt="Иллюстрация" align="left"
          vspace="5" hspace="5"/>{this.props.feed.content}</h6>
          <h6 align="right">Author: {this.props.feed.author}</h6>

          <br />
          {this.isLiked() ? (
            <i class="fas fa-heart fa-2x" onClick={this.backLike}></i>
          ) : (
              <i class="far fa-heart fa-2x" onClick={this.addToFavorite}></i>
            )}
          {this.props.feed.nrating}
          <br />
          <Comment feed={this.props.feed} />
          <div class="one">
          </div>
          <hr size="10px"/>
      </div>
        );
      }
    }
    
    
