import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect, Link, withRouter } from 'react-router-dom';
import { Collapse, Button, CardBody, Card } from 'reactstrap';
import axios, * as others from 'axios';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      email: '',
      password: '',
      redirect: false,
    };
  }

  handleEmailChange(e) {
    this.setState({ email: e.target.value })
  }
  handlePasswordChange(e) {
    this.setState({ password: e.target.value })
  }

  setRedirect = () => {
    this.setState({
      redirect: true
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    axios.post('http://localhost:8080/users/loginWithEmail',
      {
        email: this.state.email,
        password: this.state.password,
      }, {
      'Content-Type': 'application/json',
    }
    ).then((response) => {
      console.log("responce", response);
      localStorage.setItem("access_token", response.data.response.token.access_token);
      localStorage.setItem("refresh_token", response.data.response.token.refresh_token);
      localStorage.setItem("roles", response.data.response.roles);
      localStorage.setItem("userId", response.data.response.user.userId);
      localStorage.setItem("fullName", response.data.response.user.fullName);
      window.location.href = "/news";
      Auth.authenticate();
    });

  }

  render() {
    return (
      <form>
        <div class="form-group">
          <label>Email address</label>
          <input type="email" placeholder="Enter your email" name="email" onChange={this.handleEmailChange} />
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
          <label>Password</label>
          <input type="password" placeholder="Enter your password" name="password" onChange={this.handlePasswordChange} />
        </div>
        <button class="btn btn-success" onClick={this.handleSubmit}>Sign In</button> or <Link to="/register" className="FormField__Link">Create an account</Link>
      </form>
    )
  }
}

const Auth = {
  isAuthenticated: false,
  authenticate() {
    if (localStorage.getItem('access_token')) {
      this.isAuthenticated = true;
    }
  },

  signout() {
    localStorage.clear();
    this.isAuthenticated = false;
    window.location.href = "/";
  }
}