import React from 'react';
import axios from 'axios';
import Comment from './Comment';

export default class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      content: '',
      feeds: [],
      author: ''
    };
    this.handleLoadFavoriteNews = this.handleLoadFavoriteNews.bind(this);
  }

  handleLoadFavoriteNews() {
    axios.get('http://localhost:8080/feeds/favoriteNews/getAll', {
      headers: {
        'Authorization': 'bearer ' + localStorage.getItem("access_token")
      }
    }).then((resp) => {
      this.setState({
        feeds: resp.data.response.favoriteFeedList.feeds
      });
    });
  }

  componentDidMount() {
    this.handleLoadFavoriteNews();
  }

  render() {
    return (
      <div>
        <FeedList feeds={this.state.feeds} />
      </div >
    )
  }
}

class FeedList extends React.Component {
  render() {
    const feeds = this.props.feeds.map((feed) =>
      <Feed key={feed.id} feed={feed} />
    );
    return (
      <div>
        {feeds}
      </div>
    )
  }
}

class Feed extends React.Component {

  render() {
      var binary_data = this.props.feed.image;
    return (
      <div class="blog-post">
        <h3 class="ptitile">{this.props.feed.title}</h3>
        <h6 align="right">{this.props.feed.creationTime}</h6>
        <img src={`data:image/jpeg;base64,${binary_data}`} />
        <h6>{this.props.feed.content}</h6>
        <h6 align="right">Author: {this.props.feed.author.fullName}</h6>
         <i class="fas fa-heart fa-2x" onClick={this.backLike}></i>
        {this.props.feed.nRating}
        <br />
        <hr size="5"></hr>
        <Comment feed={this.props.feed} />
      </div>
    )
  }
}
