import Iframe from 'react-iframe';
import React, { Component } from 'react';


export default class Video extends React.Component{
    render() {
        return (
            < Iframe url="http://www.youtube.com/embed/xDMP3i36naA"
                width="450px"
                height="450px"
                id="myId"
                className="myClassname"
                display="initial"
                position="relative" />
        )

    }
}