import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import React from 'react';
import ReactDOM from 'react-dom';
import Proj from './Proj';

ReactDOM.render(<Proj />, document.getElementById('react'));