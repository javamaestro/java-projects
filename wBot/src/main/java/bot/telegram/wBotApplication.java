package bot.telegram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
public class wBotApplication {
    public static void main(String[] args) {
        System.getProperties().put( "proxySet", "true" );
        System.getProperties().put( "socksProxyHost", "196.38.150.104" );
        System.getProperties().put( "socksProxyPort", "8082" );
        ApiContextInitializer.init();
        SpringApplication.run(wBotApplication.class, args);
    }
}
