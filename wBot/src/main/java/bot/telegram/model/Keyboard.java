package bot.telegram.model;

import bot.telegram.bot.BotContext;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

public class Keyboard {

    public static final String TRAFFIC_BUTTON = "Пробки";
    public static final String WEATHER_BUTTON = "Погода";


    public ReplyKeyboardMarkup initKeyboard(BotContext context) {
        switch (context.getKeyboardType()) {
            case DEFAULT:
                ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
                List<KeyboardRow> keyboard = new ArrayList<>();
                KeyboardRow keyboardRow1 = new KeyboardRow();
                KeyboardRow keyboardRow2 = new KeyboardRow();
                keyboardRow1.add(new KeyboardButton(TRAFFIC_BUTTON));
                keyboardRow2.add(new KeyboardButton(WEATHER_BUTTON));
                keyboard.add(keyboardRow1);
                keyboard.add(keyboardRow2);
                replyKeyboardMarkup.setResizeKeyboard(true);
                replyKeyboardMarkup.setKeyboard(keyboard);
                return replyKeyboardMarkup;

            case WOPTIONS:
                ReplyKeyboardMarkup replyKeyboardMarkup2 = new ReplyKeyboardMarkup();
                List<KeyboardRow> keyboardWOptions = new ArrayList<>();
                KeyboardRow keyboardRowWOptions1 = new KeyboardRow();
                KeyboardRow keyboardRowWOptions2 = new KeyboardRow();
                KeyboardButton locationButton = new KeyboardButton("Отправить локацию");
                locationButton.setRequestLocation(true);
                keyboardRowWOptions2.add(locationButton);
                keyboardWOptions.add(keyboardRowWOptions1);
                keyboardWOptions.add(keyboardRowWOptions2);
                if (context.getUser().getRecentCity() != null) {
                    KeyboardRow keyboardRowWOptions3 = new KeyboardRow();
                    keyboardRowWOptions3.add(new KeyboardButton(context.getUser().getRecentCity()));
                    keyboardWOptions.add(keyboardRowWOptions3);
                }
                replyKeyboardMarkup2.setResizeKeyboard(true);
                replyKeyboardMarkup2.setKeyboard(keyboardWOptions);
                return replyKeyboardMarkup2;
        }
        return null;
    }


}
