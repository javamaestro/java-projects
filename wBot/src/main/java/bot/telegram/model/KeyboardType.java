package bot.telegram.model;

public enum KeyboardType {
    DEFAULT,
    WOPTIONS,
    OPTIONS,
    NONE
}
