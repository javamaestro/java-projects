package bot.telegram.model;

public class WeatherModel {
    private String name;
    private Double temp;
    private Double humidity;
    private String icon;
    private String main;

    public WeatherModel() {
    }

    public WeatherModel(String name, Double temp, Double humidity, String icon, String main) {
        this.name = name;
        this.temp = temp;
        this.humidity = humidity;
        this.icon = icon;
        this.main = main;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }
}
