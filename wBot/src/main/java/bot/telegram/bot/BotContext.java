package bot.telegram.bot;

import bot.telegram.model.KeyboardType;
import bot.telegram.model.User;
import org.telegram.telegrambots.meta.api.objects.Update;

public class BotContext {
    private final wBot wBot;
    private final User user;
    private final Update input;
    private KeyboardType keyboardType;

    public static BotContext of(wBot bot, User user, Update text) {
        return new BotContext(bot, user, text);
    }

    public BotContext(bot.telegram.bot.wBot wBot, User user, Update input) {
        this.wBot = wBot;
        this.user = user;
        this.input = input;
        this.keyboardType = KeyboardType.DEFAULT;
    }

    public void setKeyboardType(KeyboardType keyboardType) {
        this.keyboardType = keyboardType;
    }

    public KeyboardType getKeyboardType() {
        return keyboardType;
    }

    public wBot getwBot() {
        return wBot;
    }

    public User getUser() {
        return user;
    }

    public Update getInput() {
        return input;
    }
}
