package bot.telegram.bot;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    public static final String REGEX = "^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$";
    public static boolean isValidCityName(String city){
        Pattern r = Pattern.compile(REGEX);
        Matcher m = r.matcher(city);
        return m.find();
    }
}
