package bot.telegram.bot;

import bot.telegram.model.User;
import bot.telegram.service.UserService;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.IOException;

@Component
@Log4j2
public class wBot extends TelegramLongPollingBot {

    @Getter
    @Value("${bot.wt.username}")
    String botUsername;

    @Getter
    @Value("${bot.wt.token}")
    String botToken;

    private final UserService userService;

    public wBot(UserService userService) {
        this.userService = userService;
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        final Long chatId = update.getMessage().getChatId();
        final String userName = update.getMessage().getFrom().getFirstName();
        final Message message = update.getMessage();
        User user = userService.findByChatId(chatId);

        BotContext context;
        BotState state;

        if (user == null) {
            state = BotState.getInitialState();
            user = new User(userName, chatId, state.ordinal());
            userService.addUser(user);
            context = BotContext.of(this, user, update);
            try {
                state.enter(context);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            context = BotContext.of(this, user, update);
            state = BotState.byId(user.getStateId());
        }

        state.handleInput(context);

        do {
            state = state.nextState();
            try {
                state.enter(context);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!state.isInputNeeded());

        user.setStateId(state.ordinal());
        userService.updateUser(user);
    }

    public synchronized void sendTextMessage(Long chatId, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setText(text);

        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            log.error(e);
        }
    }


}
