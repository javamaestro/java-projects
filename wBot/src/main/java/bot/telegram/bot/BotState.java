package bot.telegram.bot;

import bot.telegram.model.Keyboard;
import bot.telegram.model.KeyboardType;
import bot.telegram.model.WeatherModel;
import bot.telegram.service.TrafficService;
import bot.telegram.service.WeatherService;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Location;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.IOException;

public enum BotState {

    Start {
        @Override
        public void enter(BotContext context) {
            sendMessage(context, "Привет, " + context.getUser().getUsername() + " я бот показывающий погоду и состояние пробок в городе!");
        }

        @Override
        public BotState nextState() {
            return Options;
        }
    },

    Options {
        private BotState next;

        @Override
        public void enter(BotContext context) {
            context.setKeyboardType(KeyboardType.DEFAULT);
            sendMessage(context, "Выбери, что тебе интересно, нажав на кнопку!");
        }

        @Override
        public void handleInput(BotContext context) {
            Update update = context.getInput();
            String command = update.getMessage().getText();
            if (command.equals("Погода") || command.equals("Пробки")) {
                switch (command) {
                    case "Погода":
                        next = WOptions;
                        break;
                    case "Пробки":
                        next = TrafficStatus;
                        break;
                }
            } else
                sendMessage(context, "Введи верную комманду!");
        }

        @Override
        public BotState nextState() {
            return next;
        }
    },
    TrafficStatus {
        @Override
        public void enter(BotContext context) throws IOException {
            sendMessage(context, "Введите название города!");
        }

        @Override
        public void handleInput(BotContext context) throws InterruptedException {
            String city = context.getInput().getMessage().getText();
            sendMessage(context, "Состояние пробок: " + new TrafficService().getTrafficByCity(city) + " баллов.");
        }


        @Override
        public BotState nextState() {
            return Options;
        }
    },
    WOptions {
        private BotState next;

        @Override
        public void enter(BotContext context) {
            context.setKeyboardType(KeyboardType.WOPTIONS);
            sendMessage(context, "Введите название города");
        }

        @Override
        public void handleInput(BotContext context) {
            Update message = context.getInput();
            Location location = message.getMessage().getLocation();
            System.out.println(message.getMessage());
            if (location != null) {
                context.getUser().setLocation(location);
                next = GetWeatherByLocation;
            } else {
                context.getUser().setRecentCity(message.getMessage().getText());
                next = GetWeather;
            }
        }

        @Override
        public BotState nextState() {
            return next;
        }

    },

    GetWeather(false) {
        private BotState next;

        @Override
        public void enter(BotContext context) throws IOException {
            String city = context.getUser().getRecentCity();
            String weather = WeatherService.getWeatherByCity(city, new WeatherModel());
            if (weather.equals("Город не найден")) {
                next = WOptions;
            } else {
                next = Options;
            }
            sendMessage(context, weather);

        }

        @Override
        public BotState nextState() {
            return Options;
        }
    },
    GetWeatherByLocation(false) {
        private BotState next;

        @Override
        public void enter(BotContext context) throws IOException {
            StringBuilder sb = new StringBuilder();
            Location city = context.getUser().getLocation();
            sb.append("lat=").append(city.getLatitude());
            sb.append("&");
            sb.append("lon=").append(city.getLongitude());
            String weather = WeatherService.getWeatherByLocation(sb.toString(), new WeatherModel());
            if (weather.equals("Город не найден")) {
                next = WOptions;
            } else {
                next = Options;
            }
            sendMessage(context, weather);
        }

        @Override
        public BotState nextState() {
            return next;
        }
    };
    private static BotState[] states;
    private final boolean inputNeeded;

    BotState() {
        this.inputNeeded = true;
    }

    BotState(boolean inputNeeded) {
        this.inputNeeded = inputNeeded;
    }

    public static BotState getInitialState() {
        return byId(0);
    }

    public static BotState byId(int id) {
        if (states == null) {
            states = BotState.values();
        }
        return states[id];
    }

    protected void sendMessage(BotContext context, String text) {
        SendMessage message = new SendMessage()
                .setChatId(context.getUser().getChatId())
                .setText(text)
                .setReplyMarkup(getCustomReplyKeyboardMarkup(context));
        try {
            context.getwBot().execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public boolean isInputNeeded() {
        return inputNeeded;
    }

    public void handleInput(BotContext context) throws InterruptedException {

    }

    public abstract void enter(BotContext context) throws IOException;

    public abstract BotState nextState();

    private ReplyKeyboardMarkup getCustomReplyKeyboardMarkup(BotContext context) {

        Keyboard keyboard = new Keyboard();
        System.out.println(context.getKeyboardType());
        return keyboard.initKeyboard(context);
    }
}
