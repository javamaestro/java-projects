package bot.telegram.service;

import bot.telegram.model.WeatherModel;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

public class WeatherService {
    public static String getWeatherByCity(String city, WeatherModel weatherModel) throws IOException {
        URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric&appid=161f1688ab81010d11e32cdde469a36f");
        StringBuilder result = new StringBuilder();
        try {
            Scanner in = new Scanner((InputStream) url.getContent());
            while (in.hasNext()) {
                result.append(in.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Город не найден");;
        }

        if (result.length() > 0) {
            JSONObject object = new JSONObject(result.toString());
            weatherModel.setName(object.getString("name"));
            JSONObject main = object.getJSONObject("main");
            weatherModel.setTemp(main.getDouble("temp"));
            weatherModel.setHumidity(main.getDouble("humidity"));

            JSONArray getArray = object.getJSONArray("weather");

            for (int i = 0; i < getArray.length(); i++) {
                JSONObject obj = getArray.getJSONObject(i);
                weatherModel.setIcon((String) obj.get("icon"));
                weatherModel.setMain((String) obj.get("main"));
            }

            return "Город: " + weatherModel.getName() + "\n" +
                    "Температура: " + weatherModel.getTemp() + "C" + "\n" +
                    "Влажность:" + weatherModel.getHumidity() + "%" + "\n" +
                    "Погода: " + weatherModel.getMain() + "\n";
        }

        return "Город не найден";


    }

    public static String getWeatherByLocation(String location, WeatherModel weatherModel) throws IOException {
        URL url = new URL("http://api.openweathermap.org/data/2.5/weather?" + location + "&units=metric&appid=161f1688ab81010d11e32cdde469a36f");
        StringBuilder result = new StringBuilder();
        try {
            Scanner in = new Scanner((InputStream) url.getContent());
            while (in.hasNext()) {
                result.append(in.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Город не найден");;
        }

        if (result.length() > 0) {
            JSONObject object = new JSONObject(result.toString());
            weatherModel.setName(object.getString("name"));
            JSONObject main = object.getJSONObject("main");
            weatherModel.setTemp(main.getDouble("temp"));
            weatherModel.setHumidity(main.getDouble("humidity"));

            JSONArray getArray = object.getJSONArray("weather");

            for (int i = 0; i < getArray.length(); i++) {
                JSONObject obj = getArray.getJSONObject(i);
                weatherModel.setIcon((String) obj.get("icon"));
                weatherModel.setMain((String) obj.get("main"));
            }

            return "Город: " + weatherModel.getName() + "\n" +
                    "Температура: " + weatherModel.getTemp() + "C" + "\n" +
                    "Влажность:" + weatherModel.getHumidity() + "%" + "\n" +
                    "Погода: " + weatherModel.getMain() + "\n";
        }

        return "Город не найден";


    }

}
