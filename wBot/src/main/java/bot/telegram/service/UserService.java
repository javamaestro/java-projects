package bot.telegram.service;

import bot.telegram.model.User;
import bot.telegram.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Transactional
    public void addUser(User user) {
        user.setAdmin(userRepository.count() == 0);
        userRepository.save(user);
    }

    @Transactional
    public  void updateUser(User user){
        userRepository.save(user);
    }

    public User findByChatId(Long chatId) {
        return userRepository.findByChatId(chatId);
    }
}
